<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{

    private $_tableSpecs = array(
        "versions" => array(
            "columns" =>
                array("name" => "varchar(255)", "version" => "varchar(15)"),
            "primary_key" => "name",
        ),
        "users"    => array(
            "columns" =>
                array("id" => "int(11) not null auto_increment", "username" => "varchar(255)", "password" => "varchar(255)", "timestamp" => "int unsigned"),
            "primary_key" => "id"
        )
    );

    protected function _initConfig()
    {
        $config = new Zend_Config($this->getOptions(), true);
        Zend_Registry::set('config', $config);
        return $config;
    }

    /**
     * Installs tables by version.
     *
     */
    protected function _initInstallVersions()
    {
        $this->bootstrap('db');
        $resource = $this->getPluginResource('db');
        $db = $resource->getDbAdapter();
        $db->query('SET CHARACTER SET \'UTF8\'');
        // First install core tables (versions should be the first table installed)
        $core_tables = explode(",", Zend_Registry::get('config')->tables->core);
        if (!$core_tables) {
            throw new Exception("No core tables specified. Please add proper code tables to configuration.");
            return false;
        }

        foreach ($core_tables as $core_table) {
            $table = new Application_Model_Table();
            foreach ($this->_tableSpecs[$core_table]["columns"] as $name => $spec) {
                $table->addColumn($name, $spec);
            }
            $table->setPrimaryKey(array($this->_tableSpecs[$core_table]["primary_key"]));
            $table->create($core_table);
        }
    }
}

