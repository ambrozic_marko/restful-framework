<?php

class UsersController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        $userSession = new Zend_Session_Namespace('user');
        $user = $userSession->user;
        if ($user) {
            $this->view->username = $user->getUsername();
        } else {
            Zend_Registry::set("Error", "Not logged in");
            $this->redirect('/users/login');
        }
    }

    public function loginAction()
    {
        $userSession = new Zend_Session_Namespace('user');
        $request = $this->getRequest();
        $form    = new Application_Form_Login();

        if ($this->getRequest()->isPost()) {
            if ($form->isValid($request->getPost())) {
                $users = new Application_Model_Users();
                $user = $users->authenticate($request->getParam("username"), $request->getParam("password"));
                if ($user) {
                    $userSession->user = $user;
                    $this->redirect('/users/index');
                } else {
                    $this->redirect('/users/login');
                }
            }
        }

        $this->view->form = $form;
    }


}



