<?php

class Application_Model_Versions
{
    protected $_dbTable;

    private $_name = "";
    private $_version = "";

    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable()
    {
        if (null === $this->_dbTable) {
            $this->setDbTable('Application_Model_DbTable_Versions');
        }
        return $this->_dbTable;
    }

    public function save()
    {
        $data = array(
            'name'   => $this->getName(),
            'version' => $this->getVersion(),
        );

        $existing = new Application_Model_Versions();
        $existing = $this->find($this->getName(), $existing);

        if (!$existing) {
            $this->getDbTable()->insert($data);
        } else {
            $this->getDbTable()->update($data, array('name = ?' => $this->getName()));
        }
    }

    public function find($name, Application_Model_Versions $version)
    {
        $result = $this->getDbTable()->find($name);
        if (0 == count($result)) {
            return;
        }
        $row = $result->current();
        $version->setName($row->name)
            ->setVersion($row->version);
    }

    public function fetchAll()
    {
        $resultSet = $this->getDbTable()->fetchAll();
        $entries   = array();
        foreach ($resultSet as $row) {
            $entry = new Application_Model_Versions();
            $entry->setName($row->name)
                ->setVersion($row->version);
            $entries[] = $entry;
        }
        return $entries;
    }

    /**
     * @return string
     */
    public function getVersion() {
        return $this->_version;
    }

    /**
     * @param string $version
     * @return Application_Model_Versions
     */
    public function setVersion($version) {
        $this->_version = $version;
        return $this;
    }

    /**
     * @return string
     */
    public function getName() {
        return $this->_name;
    }

    /**
     * @param string $name
     * @return Application_Model_Versions
     */
    public function setName($name) {
        $this->_name = $name;
        return $this;
    }


}

