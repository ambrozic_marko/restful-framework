<?php

class Application_Model_Table
{

    /** @var $_db Zend_Db_Adapter_Pdo_Mysql */
    private  $_db = null;
    private $_columns = array();
    private $_primary_keys = array();
    private $_version = '0.0.1';

    public function __construct()
    {
        $dbConfig = Zend_Registry::get('config')->resources->db->params;

        $host   = property_exists($dbConfig, "host") ? $dbConfig->host : 'localhost';
        $user   = $dbConfig->username;
        $pass   = $dbConfig->password;
        $dbName = $dbConfig->dbname;

        $options = array(
            'host'     => $host,
            'username' => $user,
            'password' => $pass,
            'dbname'   => $dbName
        );

        $this->_db = new Zend_Db_Adapter_Pdo_Mysql(
            $options
        );

    }

    public function addColumn($name, $specs) {
        $this->_columns[$name] = $specs;
    }

    /**
     * Sets primary key - only works for single primary key
     * TODO: Fix for composite key
     *
     * @param array $keys
     */
    public function setPrimaryKey(array $keys) {
        foreach ($keys as $key) {
            $this->_primary_keys[] = $key;
        }
    }

    /**
     * This should be increased if modifications have been made to the table.
     * TODO: Alter table
     *
     * @param $version
     */
    public function setVersion($version) {
        $this->_version = $version;
    }

    public function getVersion() {
        return $this->_version;
    }

    public function create($name) {
        if ($name != 'versions') {
            $existing = new Application_Model_Versions();
            $existing->find($name, $existing);
            if ($existing->getVersion() == $this->getVersion()) {
                return false;
            }
        }

        $queryString = "CREATE TABLE IF NOT EXISTS $name(";
        $binds = array($name);
        $i = 0;
        foreach ($this->_columns as $cname => $ctype) {
            if ($i) {
                $queryString .= ",";
            }
            $queryString .= "$cname $ctype";
            $binds[] = $cname;
            $binds[] = $ctype;
            $i++;
        }

        if ($this->_primary_keys) {
            $primKey = $this->_primary_keys[0];
            $queryString .= ", PRIMARY KEY ($primKey)";
            $binds[] = $this->_primary_keys[0];
        }

        $queryString .= ")";

        try {
            $stmt = $this->_db->query(
                $queryString
            );
        } catch (Exception $e) {
            print "<pre>";
            print_r($e->getMessage());
            print_r($e->getTraceAsString());
            print "</pre>";
        }

        $versions = new Application_Model_Versions();
        $versions->setName($name);
        $versions->setVersion($this->getVersion());
        $versions->save();
        return true;


    }


}

