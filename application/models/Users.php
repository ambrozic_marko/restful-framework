<?php

class Application_Model_Users
{

    protected $_username;
    protected $_password;
    protected $_timestamp;
    protected $_id;

    protected $_dbTable;

    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function getDbTable()
    {
        if (null === $this->_dbTable) {
            $this->setDbTable('Application_Model_DbTable_Users');
        }
        return $this->_dbTable;
    }

    public function save()
    {
        $data = array(
            'username'   => $this->getUsername(),
            'password' => $this->getPassword(),
            'timestamp' => date('Y-m-d H:i:s'),
        );

        if (null === ($id = $this->getId())) {
            unset($data['id']);
            $this->getDbTable()->insert($data);
        } else {
            $this->getDbTable()->update($data, array('id = ?' => $id));
        }
    }

    public function find($id, Application_Model_Users $user)
    {
        $result = $this->getDbTable()->find($id);
        if (0 == count($result)) {
            return;
        }
        $row = $result->current();
        $user->setId($row->id)
            ->setUsername($row->username)
            ->setPassword($row->password)
            ->setTimestamp($row->timestamp);
    }

    public function fetchAll()
    {
        $resultSet = $this->getDbTable()->fetchAll();
        $entries   = array();
        foreach ($resultSet as $row) {
            $entry = new Application_Model_Users();
            $entry->setId($row->id)
                ->setUsername($row->username)
                ->setPassword($row->password)
                ->setTimestamp($row->timestamp);
            $entries[] = $entry;
        }
        return $entries;
    }

    public function __construct(array $options = null)
    {
        if (is_array($options)) {
            $this->setOptions($options);
        }
    }

    public function __set($name, $value)
    {
        $method = 'set' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid user property');
        }
        $this->$method($value);
    }

    public function __get($name)
    {
        $method = 'get' . $name;
        if (('mapper' == $name) || !method_exists($this, $method)) {
            throw new Exception('Invalid user property');
        }
        return $this->$method();
    }

    public function setOptions(array $options)
    {
        $methods = get_class_methods($this);
        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($key);
            if (in_array($method, $methods)) {
                $this->$method($value);
            }
        }
        return $this;
    }

    public function setUsername($text)
    {
        $this->_username = (string) $text;
        return $this;
    }

    public function getUsername()
    {
        return $this->_username;
    }

    public function getPassword()
    {
        return $this->_password;
    }

    public function setPassword($text) {
        $this->_password = (string) $text;
        return $this;
    }

    public function setTimestamp($ts)
    {
        $this->_timestamp = $ts;
        return $this;
    }

    public function getTimestamp()
    {
        return $this->_timestamp;
    }

    public function setId($id)
    {
        $this->_id = (int) $id;
        return $this;
    }

    public function getId()
    {
        return $this->_id;
    }

    /**
     * @param $username
     * @return \Application_Model_Users
     */
    public function findByUsername($username) {
        $query = $this->getDbTable()->getAdapter()->quoteInto("username = ?", array($username));
        $row = $this->getDbTable()->fetchRow($query);

        $user = new Application_Model_Users();
        $user->setId($row['id']);
        $user->setUsername($row['username']);
        $user->setPassword($row['password']);
        $user->setTimestamp($row['timestamp']);

        return $user;
    }

    public function authenticate($username, $password) {
        $user = $this->findByUsername($username);


        if (password_verify($password, $user->getPassword())) {
            return $user;
        } else {
            return null;
        }
    }

}

